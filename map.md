# LLM Tech Map（LLM 技术图谱）


## 基础设施

### 向量数据库

- Milvus
- Pinecone
- Weaviate
- Chroma
- Qdrant
- Vespa
- Vald
- Faiss
- ScaNN
- Vearch
- AquilaDB
- Marqo
- LanceDB
- Annoy
- NucliaDB
- DeepLake
- MongoDB

### 数据库向量支持

- pgvector
- Redis Vector
- Elastic
- SingleStoreDB
- Solr 
- OpenSearch
- ClickHouse
- Rockset
- Cassandra
- Lucene
- Neo4j
- Kinetica
- Supabase
- Timescale

### 大模型框架、微调 (Fine Tuning)
        
- OneFlow
- LMFlow
- LoRA
- Alpaca-LoRA
- PEFT
- ChatGLM-Efficient-Tuning
- LLaMA-Efficient-Tuning
- P-tuning v2
- OpenLLM
- h2o-llmstudio
- xTuring
- finetuner
- YiVal

### 大模型训练平台与工具
        
- Pytorch
- BMtrain
- colossalAI
- Tensorflow
- PaddlePaddle
- MindSpore
- Deepspeed
- XGBoost
- Transformers
- Apache MXNet
- Ray

## 大模型
    
### 备案上线的中国大模型
        
- 文心一言
- 云雀
- GLM
- 紫东太初
- 百川
- 日日新
- ABAB 大模型
- 书生通用大模型
- 混元大模型

### 知名大模型
   
- Llama 2
- OpenLLaMA
- 百川
- 通义大模型
- 文心一言
- StableLM
- MOSS
- Dolly
- BLOOM
- Falcon LLM
- ChatGLM
- PaLM 2
- 盘古
- GPT-4
- Stable Diffusion
- DALL·E 3
    
### 知名大模型应用
        
- ChatGPT
- Claude
- Cursor
- Mochi Diffusion
- Midjourey
- DragGAN
- Bard
- Bing

## AI Agent (LLM Agent)
    
- Rivet
- JARVIS
- MetaGPT
- AutoGPT
- BabyAGI
- NexusGPT
- Generative Agents
- Voyager
- GPTeam
- GPT Researcher
- Amazon Bedrock Agents

## AI 编程
    
- codeium.vim
- Cursor
- GitHub Copilot
- Comate
- StableCode
- CodeGeeX
- TabbyML
- CodeArts Snap
- Code Llama
- CodeFuse
- 姜子牙

## 工具和平台
    
### LLMOps
        
- BentoML
- LangChain
- Dify.ai
- Semantic Kernel
- Arize-Phoenix 
- GPTCache
- Flowise

### 大模型聚合平台
        
- Gitee AI
- SOTA！模型
- 魔搭ModelScope
- Hugging Face
    
### 开发工具
        
- v0
- txtai
- Jina-AI
- Deco
- imgcook
- Quest AI
- CodiumAI
- Codeium Vim
- Project IDX
- MakerSuite

## 算力

- 英伟达   
- 昇腾
- AMD
- 海光
- 昆仑芯
- 天数智芯
